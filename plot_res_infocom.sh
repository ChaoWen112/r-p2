#!/bin/bash -x

plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n overall 10 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n cover 10 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n util 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n traffic 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n nact 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_n n t_plan 10 0 1

plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a overall 10 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a cover 10 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a util 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a traffic 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a nact 10 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_a mob_a t_plan 10 0 1

plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b overall 4 2 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b cover 4 2 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b util 4 2 2.4
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b traffic 4 2 2.3
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b nact 4 2 2.3
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_b mob_b t_plan 4 2 1

plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c overall 4 0 3
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c cover 4 0 0
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c util 4 0 2.75
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c traffic 4 0 2.25
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c nact 4 0 1
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_mob_c mob_c t_plan 4 0 1

plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed overall 1 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed cover 1 0 2
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed util 1 0 2.4
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed traffic 1 0 2.3
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed nact 1 0 2.3
plot_res_src/plot_res_eval_v_x.R data_infocom_csv_s_speed speed t_plan 1 0 1

plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota overall 2
plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota cover 2
plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota util 0.8
plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota traffic 0.6
plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota nact 0.8
plot_res_src/plot_res_eval_v_quota.R data_infocom_csv_s_quota quota t_plan 1
