#!/bin/bash

GEAR_RATE=1
CAP_STR="sm"
NUM_NODES=100
NUM_STATIC=60

print_usage() {
    echo >&2 "Usage: $0 DATA_QUOTA SOLUTION PROJ_ROOT OUTPUT_FILE"
}

if [ $# -lt 4 ]; then
    print_usage
    echo >&2
    echo >&2 "Illegal number of parameters"
    exit 1
fi

data_quota="$1"
solution="$2"

proj_root=$(realpath "$3")
if [ $? -ne 0 ]; then exit $?; fi
cd ${proj_root}

output_file=$(realpath -m "$4")
if [ $? -ne 0 ]; then exit $?; fi

if type module >/dev/null 2>&1; then
    module load R/3.5.0
fi

rm -f ${output_file}
touch ${output_file}
if [ $? -ne 0 ]; then exit $?; fi

grid_fn=$(realpath "prep_RData/grid_mob_one.RData")
if [ $? -ne 0 ]; then exit $?; fi

type_fn=$(realpath "prep_RData/impact_multi.RData")
if [ $? -ne 0 ]; then exit $?; fi

tail_lines=2

for case_id in `seq 1 1 5`; do
case_cap=${case_id}
case_mob=${case_id}

capacity_fn=$(realpath "prep_RData/impact_multi_capacity_${CAP_STR}_${case_cap}.RData")
if [ $? -ne 0 ]; then exit $?; fi

mobility_fn=$(realpath "prep_RData/mob_300_4_${case_mob}.RData")
if [ $? -ne 0 ]; then exit $?; fi

eval/eval.R \
--duration=10800 \
--t_frame=60 \
--data_quota=${data_quota} \
--num_nodes=${NUM_NODES} \
--num_static=${NUM_STATIC} \
--gear_rate=${GEAR_RATE} \
--grid_file=${grid_fn} \
--type_spec_file=${type_fn} \
--capacity_file=${capacity_fn} \
--placement_file=${mobility_fn} \
--solution=${solution} \
--additional_text="mob_300_4_${case_mob}_${CAP_STR}_${case_cap}" \
"${@:5}" | tail -n ${tail_lines} | tee -a "${output_file}"
if [ $? -ne 0 ]; then exit $?; fi
tail_lines=1

done

# SGE batch examples
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="fill_1";   fn="data_infocom_csv_s_quota/eval_${solution}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn}; fi; done
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="random_1"; fn="data_infocom_csv_s_quota/eval_${solution}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn} --random_seed=9; fi; done
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="greedy_2"; fn="data_infocom_csv_s_quota/eval_${solution}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn}; fi; done
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="lyap_grd"; fn="data_infocom_csv_s_quota/eval_${solution}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn} --gamma_l=1e-7; fi; done
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="greedy_4"; lh=5; fn="data_infocom_csv_s_quota/eval_${solution}_${lh}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn} --lookahead=${lh}; fi; done
# pr="$(realpath ~/proj/R-p2)"; for quota in $(cat ${pr}/eval_batch/b_infocom_s_quota_list.txt); do solution="ga_1";     nc=2; fn="data_infocom_csv_s_quota/eval_${solution}_${nc}_s_quota_${quota}.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q -pe openmpi ${nc} ~/proj/R-p2/eval_batch/b_infocom_s_quota.sh ${quota} ${solution} ${pr} ${fn} --random_seed=9 --parallel=${nc}; fi; done
