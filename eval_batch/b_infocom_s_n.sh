#!/bin/bash

GEAR_RATE=1
CAP_STR="sm"
RE_NUMBER='^[0-9]+$'

print_usage() {
    echo >&2 "Usage: $0 NUM_NODES DATA_QUOTA SOLUTION PROJ_ROOT OUTPUT_FILE"
}

if [ $# -lt 5 ]; then
    print_usage
    echo >&2
    echo >&2 "Illegal number of parameters"
    exit 1
fi

num_nodes="$1"
if [[ ! ${num_nodes} =~ ${RE_NUMBER} ]]; then
    print_usage
    echo >&2
    echo >&2 "NUM_NODES must be an integer"
    exit 1
fi

data_quota="$2"
solution="$3"

proj_root=$(realpath "$4")
if [ $? -ne 0 ]; then exit $?; fi
cd ${proj_root}

output_file=$(realpath -m "$5")
if [ $? -ne 0 ]; then exit $?; fi

if type module >/dev/null 2>&1; then
    module load R/3.5.0
fi

rm -f ${output_file}
touch ${output_file}
if [ $? -ne 0 ]; then exit $?; fi

grid_fn=$(realpath "prep_RData/grid_mob_one.RData")
if [ $? -ne 0 ]; then exit $?; fi

type_fn=$(realpath "prep_RData/impact_multi.RData")
if [ $? -ne 0 ]; then exit $?; fi

tail_lines=2

for case_id in `seq 1 1 5`; do
case_cap=${case_id}
case_mob=${case_id}

capacity_fn=$(realpath "prep_RData/impact_multi_capacity_${CAP_STR}_${case_cap}.RData")
if [ $? -ne 0 ]; then exit $?; fi

mobility_fn=$(realpath "prep_RData/mob_300_4_${case_mob}.RData")
if [ $? -ne 0 ]; then exit $?; fi

eval/eval.R \
--duration=10800 \
--t_frame=60 \
--data_quota=${data_quota} \
--num_nodes=${num_nodes} \
--num_static=$((${num_nodes} * 6 / 10)) \
--gear_rate=${GEAR_RATE} \
--grid_file=${grid_fn} \
--type_spec_file=${type_fn} \
--capacity_file=${capacity_fn} \
--placement_file=${mobility_fn} \
--solution=${solution} \
--additional_text="mob_300_4_${case_mob}_${CAP_STR}_${case_cap}" \
"${@:6}" | tail -n ${tail_lines} | tee -a "${output_file}"
if [ $? -ne 0 ]; then exit $?; fi
tail_lines=1

done

# SGE batch examples
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="fill_1";   pr="$(realpath ~/proj/R-p2)"; fn="data_infocom_csv_s_n/eval_${solution}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn}; fi; done
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="random_1"; pr="$(realpath ~/proj/R-p2)"; fn="data_infocom_csv_s_n/eval_${solution}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn} --random_seed=9; fi; done
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="greedy_2"; pr="$(realpath ~/proj/R-p2)"; fn="data_infocom_csv_s_n/eval_${solution}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn}; fi; done
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="lyap_grd"; pr="$(realpath ~/proj/R-p2)"; fn="data_infocom_csv_s_n/eval_${solution}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn} --gamma_l=1e-7; fi; done
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="greedy_4"; pr="$(realpath ~/proj/R-p2)"; lh=5; fn="data_infocom_csv_s_n/eval_${solution}_${lh}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn} --lookahead=${lh}; fi; done
# for num_nodes in $(seq 5 5 25) $(seq 30 10 200); do solution="ga_1";     pr="$(realpath ~/proj/R-p2)"; nc=2; fn="data_infocom_csv_s_n/eval_${solution}_${nc}_s_n_${num_nodes}_quota.csv"; if [ ! -f "${pr}/${fn}" ]; then qsub -q openlab.q -pe openmpi ${nc} ~/proj/R-p2/eval_batch/b_infocom_s_n.sh ${num_nodes} 3.5e+6 ${solution} ${pr} ${fn} --random_seed=9 --parallel=${nc}; fi; done
