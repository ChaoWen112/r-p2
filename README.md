# Crowdsensing Spatiotemporal Scheduling Simulator for R

This introduction page only covers initial setup steps for this project.

## Important notice about Git LFS

This project leverages [Git LFS](https://github.com/git-lfs/git-lfs/) to manage archives of old scripts, results, and plots.
In order to clone or pull this project in full, make sure Git LFS is correctly installed and initialized.

If you do not plan to contribute to this project, you may also choose to download a **manually** created project archive in [Downloads](https://bitbucket.org/bfrgbit/r-sc/downloads/).
Note that automatically created archives for branches and tags will not contain Git LFS files due to Bitbucket limitations.

## Preparing environment

### Installation of R

Refer to [Download and Install R](https://cloud.r-project.org/).
Note that if you are using Linux (e.g. Ubuntu), you will need to add the official repo of the R project. The Linux distribution's repo does not always have the latest versions of R.
How this can be done is also described in the installation procedures (the link above).

This project has been tested working with R versions **3.5.0** to **3.5.2**.

Test if your R is installed correctly: both commands below should run without any error.

```sh
$ R --version
$ Rscript --version
```

### Dependencies

R packages can be installed in an interactive R session, using
```r
install.packages()
```

This project generally depends on the following R packages:

- `optparse`: Command-line argument parser.
- `dplyr`: Data manipulation.

Some solutions may require additional packages, e.g.

- `GA`: Genetic algorithm framework, required by `solution/ga_1.R`.

Plotting batch simulation results requires the following R packages:

- `ggplot2`: Data visualization (i.e. generation of good-looking plots).
- `reshape2`: Data pre-processing.
- `Cairo`: More advanced vector graphics, used to generate PDF files with embedded fonts.

## Important notice on running R scripts

All the R scripts in this project need to be run from the **project root**.
The project root is the folder you get when you run `git clone` or when you extract this project from an archive.

Change your working directory (a.k.a. PWD) to the **project root** and run all R scripts there, even if the script itself does not reside in the project root.

## Simulations

The main script for running simulations is `eval/eval.R`.

If you are using Linux or macOS and have cloned this project with Git, this script should be executable already. Simply change into the project root and run it with `eval/eval.R`.
If it is not yet executable, you can manually change its mode or run it with `Rscript eval/eval.R`.

Let's assume that you have installed all necessary packages.
Running the script without parameters will likely result in its usage page, where you can learn how to provide the simulation with datasets.

### Datasets

Simulations require at least **four** (4) data files to run:

- **Grid specification:** Number and size of cells in the grid. It defaults to `prep_RData/grid_mob_one.RData`, which is a square cell grid we used in our INFOCOM '18 publication. The grid covers the built-in **4.5 km by 3.5 km** Helsinki city map that ships with [the ONE simulator](https://akeranen.github.io/the-one/). The default file is included in this repo.
- **Data type specification:** Data generation rates, weights, and spatiotemporal impact functions. It defaults to `prep_RData/impact_multi.RData`, which is the SCALE box setup we used in our INFOCOM '18 paper. It contains **ten** (10) data types, exponential spatial impact functions, and step-down temporal impact functions. The default file is **not** included in this repo, but can be easily generated using `misc/simu_env_prep/prep_impact_multi.R` -- of course, run it from the **project root**.
- **Node capacity:** A.k.a. sensor presence on the nodes. We have **three** (3) sets of randomly generated node capacity files matching the aforementioned data type specs, each set in a ZIP archive, e.g. `bak/prep_RData_impact_multi_capacity_sm.zip`. The "sm" set has different distribution of sensors for static and mobile nodes respectively, while the "omni" set has the same distribution for them. The "dist" is special in that it random allocates a constant amount of sensor across a varying number of nodes.
- **Node placement and mobility:** Location of static nodes and per-time-frame location of mobile nodes. Such files are usually too large to fit in the repo, so I put them in [project downloads](https://bitbucket.org/bfrgbit/r-p2/downloads/) instead. The set we used in our paper is `prep_RData_mob_walker_300_4.zip`, which contains **ten** (10) subsets of nodes and their traces, randomly picked from `mob_walker_300_4.RData` (the generation process of which is omitted for now).

The node capacity files we used can also be generated from scripts in `misc/simu_env_prep`, e.g. `prep_impact_multi_capacity_sm.R`.
The node placement and mobility files are generated by `misc/simu_env_prep/prep_placement_mob_300_4.R`, which needs `traces_RData/mob_300_4.RData` -- again, the generation process of this file is omitted for now.
The generation process of these files involves random numbers. Though we set a seed for such random processes for reproducibility, we cannot guarantee that you get exactly the same results using a different R version or on a different platform.

The four data files mentioned above are in a suit -- they share certain assumptions of the application scenario and should **not** be substituted alone, unless the substitution is from the same set of data files.

### Single-case simulation

Run a single-case simulation to make sure the environment is configured correctly and the current code-base is intact. We also run a single-case simulation as a test for newly implemented algorithms, etc.

An example of a single-case simulation command is as follows, be sure to make sure all files are present and run the command from the **project root**.

```sh
$ eval/eval.R \
--duration=300 \
--t_frame=60 \
--capacity_file=prep_RData/impact_multi_capacity_sm_4.RData \
--placement_file=prep_RData/mob_300_4_4.RData \
--solution=greedy_4 \
--lookahead=5 \
--data_quota=3.5e+6 \
--num_nodes=10 \
--num_static=6 \
--period=1 \
--verbose
```

Note that certain command-line parameters are only supported by some solution algorithms. For example, `lookahead` is used in the HSF+ algorithm (i.e. `solution/greedy_4.R`).

### Batches

With batch simulations, we can run the test sets we designed for performance evaluation of our algorithms.

Examples are given to reproduce the INFOCOM '18 paper results.
Each of the shell scripts in `eval_batch` runs the simulation **five** (5) times using **five** (5) different combinations of randomly generated capacity and placement profiles, and generates a "data point" in its corresponding final plot.
Each script is named after the test set it handles, e.g. `b_infocom_s_mob_b.sh` runs the "mob_b" test set, where the independent variable is the number of mobile nodes, and the number of static nodes is fixed.
Each script needs some command-line arguments, where we specify the value of the independent variable, the algorithm to use, the path of the output file (CSV format), etc.
Note that in each of such scripts, at the end of the file we have some commented lines of examples. These are the SGE `qsub` examples to submit multiple such batch jobs together.

The following one is such an example command for the "mob_b" test set:

```sh
$ for num_mob in $(seq 0 2 50); do \
    solution="greedy_4"; \
    pr="$(realpath ~/proj/R-p2)"; \
    lh=5; \
    fn="data_infocom_csv_s_mob_b/eval_${solution}_${lh}_s_mob_b_${num_mob}_quota.csv"; \
    if [ ! -f "${pr}/${fn}" ]; then \
        qsub -q openlab.q \
        ~/proj/R-p2/eval_batch/b_infocom_s_mob_b.sh ${num_mob} \
            3.5e+6 \
            ${solution} \
            ${pr} \
            ${fn} \
            --period=1 \
            --lookahead=${lh}; \
    fi; \
done
```

It runs `eval_batch/b_infocom_s_mob_b.sh` for **twenty-six** (26) different number of mobile nodes (0, 2, 4, ..., 50) using the HSF+ algorithm (i.e. "greedy_4") with lookahead window of **five** (5) timeframes, and redirects the script's output to a CSV file.
Note that the INFOCOM plot scripts will expect the names of the CSV files to follow certain patterns.

If not using SGE, replace the `qsub -q openlab.q` command accordingly.

## Plots
 
The file `plot_res_infocom.sh` contains 36 commands that generates **thirty-six** (36) plots for the INFOCOM '18 simulations -- **six** (6) test sets with different independent variables, multiplied by **six** (6) metrics for each set.

Involved R scripts include `plot_res_src/plot_res_eval_v_quota.R` which uses a logarithmic scale for the x-axis if the independent variable is data quota, and `plot_res_src/plot_res_eval_v_x.R` which uses a linear scale for the x-axis if the independent variable is **not** data quota.
Auxiliary information and configuration such as label captions, themes, etc. are in `lib/plot_aux.R` and `lib/plot_theme.R`.

If you are extending the experiments for different test sets, you should probably create a new shell script, and enhance the R scripts if necessary.

## Reproducing the INFOCOM '18 paper results

The original INFOCOM '18 paper results are kept in this repo, under path `bak`, named `eval_data_20190000.tgz` (for all results in RData format) and `results_20190000.tgz` (for all plots in PDF format).

In an effort to extend our work, we conducted refactoring on this repository and changed the interface to call batch simulations, which resulted in the aforementioned procedures.
With the 2019 version of the simulation codes, we reproduced the INFOCOM '18 paper results and verified that they are exactly the same with the results we published in the paper, except that:

- Planning time is measured in real-world time, which is machine-dependent.
- We did not plot the GA results, because we found this method does not do the job well. 
- Our GA implementation has a termination condition to make sure planning time be less than the length of a timeframe, which leads to machine-dependent performance.

Our reproduced INFOCOM '18 paper results are also kept in this repo, under path `bak`, named `data_infocom_csv_s_*.tgz` (for all results in CSV format) and `plot_infocom_20190312.tgz` (for all plots in PDF format).

If you would like to reproduce the INFOCOM '18 paper results, there are certain tips that may help avoid inconsistency.
It is recommended that you try to reproduce our results using the same datasets to make sure that your results are exactly the same with ours (same exceptions apply as mentioned above) before attempting any modification or extension.

### Datasets

First of the first, if you would like to reproduce our results, you need to use the same datasets as we used.
There are four input sets of data, which are already mentioned earlier in this README file. As a summary, the datasets we used are:

- **Grid specification:** `prep_RData/grid_mob_one.RData`; this file is _included_ in this repo at the time we composed this README.
- **Data type specification:** `prep_RData/impact_multi.RData`; this file needs to be _generated_ using an included script at `misc/simu_env_prep/prep_impact_multi.R`.
- **Node capacity:** `prep_RData/impact_multi_(capacity_sm|cap_omni|cap_dist)_*.RData` (**three** sets, each with **ten** files, named in **three** groups noted in parentheses); these files should be _extracted_ (in the **project root**) from archive files at `bak/prep_RData_impact_multi_(capacity_sm|cap_omni|cap_dist).zip` for consistency seek. Note that they were generated using **three** (3) scripts at `misc/simu_env_prep/prep_impact_multi_(capacity_sm|cap_omni|cap_dist).R`, but the generation process involved randomness, so extraction is suggested for reproduction purpose.
- **Node placement and mobility:** `prep_RData/mob_300_4_*.RData` (**ten** of them, but only the first **five** are used); these files should be _extracted_ (in the **project root**) from the downloaded archive file [prep_RData_mob_walker_300_4.zip](https://bitbucket.org/bfrgbit/r-p2/downloads/prep_RData_mob_walker_300_4.zip).

### Batches and parameters

We used the batch files at `eval_batch/b_infocom_s_*.sh` to run simulation cases in batches.
Each of these files should have a few commented lines at the end, which are example commands to run batches. Each line of them is for one (1) solution (planning algorithm).

Of course, in order to reproduce the results, the parameters must also match ours.
Note that certain algorithms require additional parameters.
Therefore, if you attempt to create a more "comprehensive" batch script to automate the command invocation, do not forget these parameters:

- Random, i.e. `random`, needs a random seed, i.e. `random_seed`, to create reproducible results; we used `9`.
- Lyapunov, i.e. `lyap_grd`, needs a coefficient `gamma_l` for the Lyapunov drift term; we used `1e-7`.
- HSF+P, i.e. `greedy_4`, needs a lookahead window, i.e. `lookahead`; we used `0`, `5`, `10`, `15` for four sets of results.
- Lyapunov+P, i.e. `lyap_grd_4`, also needs a lookahead window. 

### Results in CSV format

Before plotting the results, make sure you validate the CSV files -- each file resulted from our batches should contain 6 lines: 1 header line and 5 data lines. If it contains a different number of lines or contains more than one header lines, you should delete the CSV file and run the corresponding test case again to obtain the correct one.

The aforementioned batch execution command contains a `-f` routine to avoid running the test cases that already have corresponding data files. Read it!
Example validation command that prints the names and then removes all CSV files in the PWD that contains a different number of lines from **six** (6):

```sh
for fn in $(ls *.csv); do \
    nl=$(wc -l ${fn} | cut -f 1 -d " "); \
    if [ ${nl} -ne 6 ]; then \
        echo ${nl} ${fn}; \
        rm ${fn}; \
    fi; \
done
```

Example validation command that prints the names and then removes all CSV files in the PWD that contains more than **one** (1) headers:

```sh
for fn in $(ls); do \
    nl=$(cat ${fn} | grep solution | wc -l); \
    if [ ${nl} -gt 1 ]; then \
        echo ${nl} ${fn}; \
        rm ${fn}; \
    fi; \
done
```

The plotting script should not output any error when producing the plots.
If there are errors, check carefully if the CSV files are named correctly and contain the correct number of lines.
