# prep_impact_multi_capacity_omni.R
#
# Created: 2017-5-17
#  Author: Charles Zhu
#
rm(list = ls())

require(methods)

# general simulation SETTINGS
num_nodes = 1800L
# num_static = 1200L
# num_mob = num_nodes - num_static

# mode-specific simulation SETTINGS
val_k_humid = 1L
val_k_temp = 1L
val_k_pm25 = 1L
val_k_pm10 = 1L
val_k_pm1 = 1L
val_k_audio = 1L
val_k_photo = 1L
lockBinding("val_k_humid", globalenv())
lockBinding("val_k_temp", globalenv())
lockBinding("val_k_pm25", globalenv())
lockBinding("val_k_pm10", globalenv())
lockBinding("val_k_pm1", globalenv())
lockBinding("val_k_audio", globalenv())
lockBinding("val_k_photo", globalenv())

num_types = 7L

p_humid_mob = c(audio_1 = 0.6)
p_temp_mob = c(audio_1 = 0.6)
p_pm25_mob = c(audio_1 = 0.6)
p_pm10_mob = c(audio_1 = 0.6)
p_pm1_mob = c(audio_1 = 0.6)
p_audio_mob = c(audio_1 = 0.6)
p_photo_mob = c(photo_1 = 0.9)
lockBinding("p_humid_mob", globalenv())
lockBinding("p_temp_mob", globalenv())
lockBinding("p_pm25_mob", globalenv())
lockBinding("p_pm10_mob", globalenv())
lockBinding("p_pm1_mob", globalenv())
lockBinding("p_audio_mob", globalenv())
lockBinding("p_photo_mob", globalenv())

source("lib/element_multi_2.R")

seeds = 1L:10L
for(seed in seeds) {
    cat(
        sprintf("Make random capacity seed = %d\n", seed)
    )
    set.seed(seed)
    capacity_mat = get_capacity_mat_multi_omni(
        val_n           = num_nodes,
        val_k_humid     = val_k_humid,
        val_k_temp      = val_k_temp,
        val_k_pm25      = val_k_pm25,
        val_k_pm10      = val_k_pm10,
        val_k_pm1       = val_k_pm1,
        val_k_audio     = val_k_audio,
        val_k_photo     = val_k_photo,
        p_humid         = p_humid_mob,
        p_temp          = p_temp_mob,
        p_pm25          = p_pm25_mob,
        p_pm10          = p_pm10_mob,
        p_pm1           = p_pm1_mob,
        p_audio         = p_audio_mob,
        p_photo         = p_photo_mob
    )
    capacity_omni = capacity_mat

    save(
        capacity_omni,
        p_humid_mob, p_temp_mob, p_pm25_mob, p_pm10_mob, p_pm1_mob, p_audio_mob, p_photo_mob,
        file = sprintf("prep_RData/impact_multi_cap_omni_%d.RData", seed)
    )
}
