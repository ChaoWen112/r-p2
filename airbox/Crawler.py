import requests
import json
import os
import logging
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import time
import datetime

# Sensors Types
#    "s_g8": "TVOC (ppb)",
#    "s_b0": "barometric pressure (mmHg)",
#    "s_t0": "Temperature (degree, C)"
#    "s_d2": "PM1 (ug/m3)",
#    "s_g8e": "CO2 equivalent (ppb)",
#    "s_d0": "PM2.5 (ug/m3)",
#    "s_d1": "PM10 (ug/m3)",
#    "s_h0": "Relative humidity (%)",
#    "s_gg": "CO2 (ppm)"

class Crawler:
    def __init__(self, *args, **kwargs):
        self.airbox = []
        self.map = []
        self.lass = []
        self.lass4u = []
        self.eevee = []
        self.indie = []
        self.probecube = []
        self.airQ = []
        self.airU = []
        self.korea = []
        self.history = {'device':[]}
        self.sensorType = ["s_g8","s_b0","s_t0","s_d2","s_g8e","s_d0","s_d1","s_h0","s_gg"]
        self.datasetB = ["newtaipei.json", "taichung.json", "tainan.json", "kaohsiung.json"]
        self.heatmap = [[[] for i in range(400)] for j in range(400)]
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s : %(message)s', filename='crawlerLog.txt')
        return super().__init__(*args, **kwargs)
    def listObj(self,o):
        pass
    def loadData(self, url, name):
        req = requests.get(url)
        with open(name+'json', 'w') as jsonFile:
            json.dump(req.json(), jsonFile)
            os.chmod(name+'json', 0o775)
    def lastData(self, id):
        return requests.get("https://pm25.lass-net.org/data/last.php?device_id="+id).json()
    def loadAllAirQ(self):
        res = requests.get("https://air.eng.utah.edu/dbapi/api/liveSensors/all")
        self.allAirQ = res.json()
    def loadAirbox(self, cache):
        if(cache):
            with open('airbox.json') as jsonFile:
                self.airbox = json.load(jsonFile)
                return
        # Dataset A
        req = requests.get("https://pm25.lass-net.org/data/last-all-airbox.json")
        self.airbox = req.json()
        # Dataset B
        for city in self.datasetB:
            req = requests.get("https://pm25.lass-net.org/AirBox/" + city)
            datasetB = req.json()
            for idx, id in enumerate(datasetB['feeds']):
                logging.info('loading datasetB ' + id["site_id"])
                data = self.lastData(id["site_id"])
                if(len(data['feeds']) and 'AirBox2' in data['feeds'][0]):
                    datasetB['feeds'][idx].update(data['feeds'][0]['AirBox2'])
                elif(len(data['feeds']) and 'AirBox' in data['feeds'][0]):
                    datasetB['feeds'][idx].update(data['feeds'][0]['AirBox'])
                else:
                    logging.info('dead device: ' + id["site_id"])
            self.airbox['feeds'] += datasetB['feeds']
        with open('airbox.json', 'w') as jsonFile:
           json.dump(self.airbox, jsonFile)
           os.chmod('airbox.json', 0o775)
    def loadMap(self, cache):
        if(cache):
            with open('map.json') as jsonFile:
                self.map = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/data/last-all-maps.json")
        self.map = req.json()
        with open('map.json', 'w') as jsonFile:
           json.dump(self.map, jsonFile)
           os.chmod('map.json', 0o775)
    def loadLass(self, cache):
        if(cache):
            with open('lass.json') as jsonFile:
                self.lass = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/data/last-all-lass.json")
        self.lass = req.json()
        req = requests.get("https://pm25.lass-net.org/data/last-all-lass4u.json")
        self.lass['num_of_records'] += req.json()['num_of_records']
        self.lass['feeds'] += req.json()['feeds']
        with open('lass.json', 'w') as jsonFile:
           json.dump(self.lass, jsonFile)
           os.chmod('lass.json', 0o775)
    def loadEevee(self, cache):
        if(cache):
            with open('eevee.json') as jsonFile:
                self.eevee = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/data/last-all-eevee.json")
        self.eevee = req.json()
        with open('eevee.json', 'w') as jsonFile:
           json.dump(self.eevee, jsonFile)
           os.chmod('eevee.json', 0o775)
    def loadIndie(self, cache):
        if(cache):
            with open('indie.json') as jsonFile:
                self.indie = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/data/last-all-indie.json")
        self.indie = req.json()
        with open('indie.json', 'w') as jsonFile:
           json.dump(self.indie, jsonFile)
           os.chmod('indie.json', 0o775)
    def loadProbecube(self, cache):
        if(cache):
            with open('probecube.json') as jsonFile:
                self.probecube = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/data/last-all-probecube.json")
        self.probecube = req.json()
        with open('probecube.json', 'w') as jsonFile:
           json.dump(self.probecube, jsonFile)
           os.chmod('probecube.json', 0o775)
    def loadAirQ(self, cache):
        if(cache):
            with open('airQ.json') as jsonFile:
                self.airQ = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/AirBox/other-airq.json")
        self.airQ = req.json()
        for idx, id in enumerate(self.airQ['feeds']):
            logging.info('loading AirQ' + id["site_id"])
            data = self.lastData(id["site_id"])
            self.airQ['feeds'][idx]['s_d0'] = data['feeds'][0]['AirQ']['s_d0']
        with open('airQ.json', 'w') as jsonFile:
           json.dump(self.airQ, jsonFile)
           os.chmod('airQ.json', 0o775)
    def loadAirU(self, cache):
        if(cache):
            with open('airU.json') as jsonFile:
                self.airU = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/AirBox/other-airu.json")
        self.airU = req.json()
        for idx, id in enumerate(self.airU['feeds']):
            logging.info('loading AirU' + id["site_id"])
            data = self.lastData(id["site_id"])
            self.airU['feeds'][idx]['s_d0'] = data['feeds'][0]['AirU']['s_d0']
        with open('airU.json', 'w') as jsonFile:
           json.dump(self.airU, jsonFile)
           os.chmod('airU.json', 0o775)
           
    def loadKorea(self, cache):
        if(cache):
            with open('korea.json') as jsonFile:
                self.korea = json.load(jsonFile)
                return
        req = requests.get("https://pm25.lass-net.org/AirBox/korea.json")
        self.korea = req.json()
        for idx, id in enumerate(self.korea['feeds']):
            logging.info('loading Korea' + id["site_id"])
            data = self.lastData(id["site_id"])
            self.korea['feeds'][idx].update(data['feeds'][0]['AirBoxK'])
        with open('korea.json', 'w') as jsonFile:
           json.dump(self.korea, jsonFile)
           os.chmod('korea.json', 0o775)

    def loadHistory(self, deviceId):
        logging.info('loading history of device ' + deviceId)
        url = "https://pm25.lass-net.org/data/history.php?device_id="+deviceId
        res = requests.get(url).json()
        self.history['device'] += res['feeds']

    def loadAirboxHistory(self):
        if(len(self.airbox)):
            for a in self.airbox['feeds']:
                if('device_id' not in a):
                    continue
                self.loadHistory(a['device_id'])
        else:
            return
        with open('airboxHistory.json', 'w') as jsonFile:
           json.dump(self.history, jsonFile)
           os.chmod('airboxHistory.json', 0o775)

    def loadLassHistory(self):
        if(len(self.lass)):
            for l in self.lass['feeds']:
                self.loadHistory(l['device_id'])
        else:
            return
        with open('lassHistory.json', 'w') as jsonFile:
           json.dump(self.history, jsonFile)
           os.chmod('lassHistory.json', 0o775)

    def loadAirQHistory(self):
        if(len(self.airQ)):
            for a in self.airQ['feeds']:
                self.loadHistory(a['site_id'])
        with open('airQHistory.json', 'w') as jsonFile:
           json.dump(self.history, jsonFile)
           os.chmod('airQHistory.json', 0o775)

    def loadAirUHistory(self):
        if(len(self.airU)):
            for a in self.airU['feeds']:
                self.loadHistory(a['site_id'])
        with open('airUHistory.json', 'w') as jsonFile:
           json.dump(self.history, jsonFile)
           os.chmod('airUHistory.json', 0o775)

    def loadKoreaHistory(self):
        if(len(self.airU)):
            for k in self.airU['feeds']:
                self.loadHistory(k['site_id'])
        with open('koreaHistory.json', 'w') as jsonFile:
           json.dump(self.history, jsonFile)
           os.chmod('koreaHistory.json', 0o775)

    def loadHistoryFromJson(self, filename):
        with open(filename, 'r') as file:
            self.history['device'] = json.load(file)

    def mapping(self):
        for h in self.history['device']:
            app = next(iter(h))
            for data in h[app]:
                time = next(iter(data))
                lat = int((data[time]['gps_lat']-22)/0.01)
                lon = int((data[time]['gps_lon']-120)/0.01)
                if(lat >= 400 or lat < 0 or lon >= 400 or lon < 0):
                    continue
                logging.info('insert into map[%d][%d]' %(lat, lon))
                self.map[lat][lon].append(data[time])
        with open('heatmap.list', 'wb') as fp:
            pickle.dump(self.map, fp)

    def plotGPS(self):
        x = []
        y = []
        for lat, h in enumerate(self.heatmap):
            for lon, t in enumerate(h):
                if(len(t)):
                    x.append(lon)
                    y.append(lat)
        plt.plot(x,y,'ro')
        plt.show()

    def refresh(self):
        self.loadAirbox(0)
        self.loadMap(0)
        self.loadLass(0)
        self.loadEevee(0)
        self.loadIndie(0)
        self.loadProbecube(0)
        self.loadAirQ(0)
        self.loadAirU(0)
        self.loadKorea(0)
        pass
    def loadJson(self):
        self.loadAirbox(1)
        self.loadLass(1)
        self.loadAirQ(1)
        self.loadAirU(1)
        self.loadKorea(1)
        pass

    def loadAllHistory(self):
        self.loadAirboxHistory()
        self.loadLassHistory()
        self.loadAirQHistory()
        self.loadAirUHistory()
        self.loadKoreaHistory()
        pass

    def removeNA(self, data):
        return [i for i in data if i['gps_lon'] != 'N/A' and i['gps_lat'] != 'N/A' and i['gps_lon'] != '0' and i['gps_lat'] != '0']
    def reserveGPS(self, data, lon_min, lon_max, lat_min, lat_max):
        return [i for i in data if i['gps_lon'] >= lon_min and i['gps_lon'] < lon_max and i['gps_lat'] >= lat_min and i['gps_lat'] < lat_max]

    def plotHeatmap(self, data, lon_min, lon_max, lat_min, lat_max, precision):
        print(len(data))
        data = self.removeNA(data)
        data = self.reserveGPS(data, lon_min, lon_max, lat_min, lat_max)
        print(len(data))
        quantity = [[0 for i in range(int((lon_max-lon_min)/precision)+1)] for j in range(int((lat_max - lat_min)/precision)+1)]
        heatmap = [[0 for i in range(int((lon_max-lon_min)/precision)+1)] for j in range(int((lat_max - lat_min)/precision)+1)]
        for i in data:
            #print(i['gps_lon'], i['gps_lat'])
            y = int((i['gps_lon'] - lon_min) / precision)
            x = int((i['gps_lat'] - lat_min) / precision)
            #print(x, y)
            heatmap[x][y] += 1#(heatmap[x][y] * quantity[x][y] + i['s_d0']) / (quantity[x][y] + 1)
            #quantity[x][y] = quantity[x][y] + 1
        heatmap = np.array(heatmap)
        heatmap_masked = np.ma.masked_where(heatmap == 0, heatmap)
        sns.set()
        fig, ax = plt.subplots(figsize=(10,10)) 
        sns.heatmap(heatmap_masked, vmin=0, vmax=100, cmap='RdPu', square=True, annot=True, fmt='.1f', annot_kws={"size": 5},  ax = ax)
        ax.invert_yaxis()
        plt.savefig('output/heatmap.eps', format='eps')
        plt.show()
    def calculate_GPS(self, lat1, lon1, lat2, lon2):
        # approximate radius of earth in km
        R = 6373.0

        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        return  R * c
    def plot_distance_error(self, data): 
        # e.g. data = airbox['feeds']
        dist = []
        error = []
        for a in data[100:199]:
            for b in data[100:199]:
                if a != b:
                    distance = calculate_GPS(a['gps_lat'], a['gps_lon'], b['gps_lat'], b['gps_lon'])
                    if(distance > 400):
                        continue
                    dist.append(calculate_GPS(a['gps_lat'], a['gps_lon'], b['gps_lat'], b['gps_lon']))
                    error.append(abs(a['s_d0'] - b['s_d0']))
        dist = np.array(dist)
        error = np.array(error)
        avg = [[] for i in range(400)]

        mean = [1 for i in range(400)]
        len(avg)
        for idx, d in enumerate(dist):
            avg[int(d)].append(error[idx])
        for idx, l in enumerate(avg):
            l = np.array(l)
            if(len(l)):
                mean[idx] = l.mean()
        len(mean)
        fig,ax = plt.subplots()

        plt.plot(dist, error,'o', markersize=1, label='error')
        plt.plot([i for i in range(400)], mean, label='Mean', linestyle='--')
        plt.xlabel('distance (km)')
        plt.ylabel('pm2.5 error')
        # Make a legend
        legend = ax.legend(loc='upper right')
        plt.savefig('output/distance-error-2.eps', format='eps')
        plt.show()

    def plot_time_error(self, data): # e.g. data = history['device'][0]['AirBox']
        timestamp = []
        value = []
        time_error = []
        data_error = []
        for i in data:
            timestamp.append(
                int(time.mktime(datetime.datetime.strptime(i[list(i.keys())[0]]['timestamp'], "%Y-%m-%dT%H:%M:%SZ").timetuple())))
            value.append(i[list(i.keys())[0]]['s_d0'])
        basetime = timestamp[-1]
        basedata = value[-1]
        print("basetime = %d, basedata = %d" %(basetime, basedata))

        for idx, d in enumerate(value):
            data_error.append(abs(d-basedata))
            time_error.append(abs(timestamp[idx]-basetime))
            
        fig,ax = plt.subplots()

        plt.plot(time_error, data_error,'o', markersize=1, label='error')
        plt.title('ID: ' + data[0][list(data[0].keys())[0]]['device_id'])
        plt.xlabel('time (second)')
        plt.ylabel('pm2.5 error')
        # Make a legend
        legend = ax.legend(loc='upper right')
        plt.savefig('output/time-error-0.eps', format='eps')
        plt.show()

    def __str__(self):
        pass


#data = json.load(jsonFile)
if __name__ == "__main__":
    cr = Crawler()
    cr.refresh()
    cr.loadAllHistory()
    #cr.plotHeatmap(cr.airbox['feeds'], 120, 122, 22, 26, 0.1)
    pass

# Taipei: 339
# Taichung: 
location = {
    'Taipei': [[24.5, 121.45], [25.01, 121.63]],
    'Taichung': [[24.05, 120.52],[24.25,]]
}
